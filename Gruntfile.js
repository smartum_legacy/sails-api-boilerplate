'use strict';

module.exports = (grunt) => {
  let includeAll;

  try {
    includeAll = require('include-all');
  } catch (e0) {
    try {
      includeAll = require('sails/node_modules/include-all');
    } catch(e1) {
      console.error('Could not find `include-all` module.');
      console.error('Skipping grunt tasks...');
      console.error('To fix this, please run:');
      console.error('npm install include-all --save`');
      console.error();

      grunt.registerTask('default', []);

      return;
    }
  }

  let loadTasks = (relPath) => {
    return includeAll({
      dirname: require('path').resolve(__dirname, relPath), filter: /(.+)\.js$/
    }) || {};
  }

  let invokeConfigFn = (tasks) => {
    for (let taskName in tasks) {
      if (tasks.hasOwnProperty(taskName)) {
        tasks[taskName](grunt);
      }
    }
  }

  let taskConfigurations = loadTasks('./tasks/config'),
      registerDefinitions = loadTasks('./tasks/register');

  if (!registerDefinitions.default) {
    registerDefinitions.default = (grunt) => {
      grunt.registerTask('default', []);
    };
  }

  invokeConfigFn(taskConfigurations);
  invokeConfigFn(registerDefinitions);
};
