module.exports = (grunt) => {
  grunt.config.set('watch', {
    api: {
      files: ['api/**/*', '!**/node_modules/**']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
};
