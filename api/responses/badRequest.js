'use strict';

module.exports = (data, options) => {
  let req = this.req;
  let res = this.res;
  let sails = req._sails;

  res.status(400);

  if (data !== undefined) {
    sails.log.verbose('Sending 400 ("Bad Request") response: \n', data);
  } else {
    sails.log.verbose('Sending 400 ("Bad Request") response');
  }

  if (sails.config.environment === 'production') {
    data = undefined;
  }

  return res.jsonx(data);
};
