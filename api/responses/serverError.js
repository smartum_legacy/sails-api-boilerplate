'use strict';

module.exports = (data, options) => {
  let req = this.req;
  let res = this.res;
  let sails = req._sails;

  res.status(500);

  if (data !== undefined) {
    sails.log.error('Sending 500 ("Server Error") response: \n',data);
  } else {
    sails.log.error('Sending empty 500 ("Server Error") response');
  }

  if (sails.config.environment === 'production') {
    data = undefined;
  }

  return res.jsonx(data);
};
