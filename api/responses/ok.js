'use strict';

module.exports = (data, options) => {
  let res = this.res;
  let sails = this.req._sails;

  sails.log.silly('res.ok() :: Sending 200 ("OK") response');

  res.status(200);

  return res.jsonx(data);
};
